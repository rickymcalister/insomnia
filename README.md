Insomnia
========

Requirements
------------

* [xdotool](http://www.semicomplete.com/blog/projects/xdotool/)

Usage
-----

Clone the [repository](https://bitbucket.org/rickymcalister/insomnia/overview)

	git clone https://bitbucket.org/rickymcalister/insomnia.git

Execute the script

	./insomnia/keepmeawake
